﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;
using DBSizeCalculator.Properties;

namespace DBSizeCalculator
{
   //How to calculate database size taken from:
   //https://technet.microsoft.com/en-us/library/ms178085(v=sql.110).aspx
   public partial class Form1 : Form
   {
      private ICollection<Table> tables;
      private const int FILL_FACTOR = 99; // database fill factor default value
      private const int DEFAULT_NUMBER_OF_ROWS = 1000; // default number of rows  
      private const string LOCAL_DB_CS = @"Data Source=(localdb)\Projects;Initial Catalog=BatApp;Integrated Security=True"; // default connectionstring 

      //Data types wit fixed size
      private string[] fixedTypes =
      {
         "bigint",
         "int",
         "smallint",
         "tinyint",
         "bit",
         "numeric",
         "decimal",
         "dec",
         "money",
         "smallmoney",
         "float",
         "real",
         "datetime",
         "smalldatetime",
         "char",
         "nchar",
         "binary",
         "uniqueidentifier",
         "timestamp",
         "rowversion"
      };

      public Form1()
      {
         InitializeComponent();
         tables = new List<Table>();
         numericUpDownRows.Value = DEFAULT_NUMBER_OF_ROWS;
         textBox1.Text = LOCAL_DB_CS;

         comboBox1.Items.Add("Bytes");
         comboBox1.Items.Add("Kilobytes");
         comboBox1.Items.Add("Megabytes");
         comboBox1.Items.Add("Gigabytes");
         numericUpDown1.Value = FILL_FACTOR;
         comboBox1.SelectedIndex = 1;
      }


      //Loading data from DB
      private void button1_Click(object sender, EventArgs e)
      {
         //clear data
         listBoxTables.Items.Clear();
         tables.Clear();
         try
         {
            SqlConnection sc = new SqlConnection(textBox1.Text);
            List<string> result = new List<string>();

            sc.Open();
             SqlCommand cmd = new SqlCommand(@"SELECT name FROM sys.Tables", sc);
            DataTable schemaTable;
            var myReader = cmd.ExecuteReader();
            //Get list of tables in db
            while (myReader.Read())
            {
               result.Add(myReader["name"].ToString());
            }
            myReader.Close();
            //Get information about tables
             foreach (var tableName in result)
             {
                 Table table = new Table();
                 table.Name = tableName;
                 table.RowsCount = DEFAULT_NUMBER_OF_ROWS;
                 table.Columns = new List<Column>();
                 cmd.Connection = sc;
                 cmd.CommandText = "SELECT * FROM " + tableName;
                 myReader = cmd.ExecuteReader(CommandBehavior.KeyInfo);

                 //Retrieve column schema into a DataTable.
                 schemaTable = myReader.GetSchemaTable();

                 //Load informations about 
                 if (schemaTable != null)
                     foreach (DataRow myField in schemaTable.Rows)
                     {
                         Column column = new Column();
                         column.Name = myField[schemaTable.Columns["ColumnName"]].ToString();
                         column.DataType = myField[schemaTable.Columns["DataTypeName"]].ToString();
                         int columnSize = int.Parse(myField[schemaTable.Columns["ColumnSize"]].ToString());
                         column.BaseSizeOfColumn = columnSize;
                         column.EditedSizeOfColumn = (columnSize > 2000 ? 150 : 0);
                         table.Columns.Add(column);
                     }
                 tables.Add(table);
                 listBoxTables.Items.Add(table.Name);
                 myReader.Close();
                 listBoxTables.SelectedIndex = 0;
             }

             UpdateTexts();
         }
         catch (Exception ex)
         {
            MessageBox.Show(Resources.EXCEPTION + ex.Message);
         }
      }


      /// <summary>
      /// Changed selected table
      /// </summary>
      private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
      {
         Table table = tables.ElementAt(listBoxTables.SelectedIndex);
         dataGridViewColumns.Rows.Clear();
         foreach (var column in table.Columns)
         {
             dataGridViewColumns.Rows.Add(column.Name, column.DataType, column.BaseSizeOfColumn, column.EditedSizeOfColumn);
         }
         UpdateTexts();
         numericUpDownRows.Value = table.RowsCount;

         groupBox1.Visible = true;
         groupBox1.Text = table.Name;
      }

      /// <summary>
      /// Calculate row size for all tables
      /// </summary>
      private void CalculateRowSize()
      {
         foreach (var table in tables)
         {
            long fixedDataSize = 0;
            long variableDataSize = 0;

            long fixedDataSizeComputed = 0;
            long variableDataSizeComputed = 0;

            int variableCount = 0;

            foreach (var column in table.Columns)
            {

               if (fixedTypes.Contains(column.DataType))
               {
                  fixedDataSize += column.BaseSizeOfColumn;
                  fixedDataSizeComputed += column.EditedSizeOfColumn > 0 ? column.EditedSizeOfColumn : column.BaseSizeOfColumn;
               }
               else
               {
                  variableDataSize += column.BaseSizeOfColumn;
                  variableDataSizeComputed += column.EditedSizeOfColumn > 0
                     ? column.EditedSizeOfColumn
                     : column.BaseSizeOfColumn;
                  variableCount++;
               }
            }

            double nullBitmap = 2 + ((table.Columns.Count + 7)/8);

            //Calcualte size of variable types
            variableDataSize = 2 + (variableCount * 2) + variableDataSize;
            variableDataSizeComputed = 2 + (variableCount * 2) + variableDataSizeComputed;

            //Calculate row size
            table.RowSize = fixedDataSize + variableDataSize + (long) nullBitmap + 4;
            table.RowSizeComputed = fixedDataSizeComputed + variableDataSizeComputed + (long) nullBitmap + 4;

         }
      }



      //Calculate table size for single table
      private void CalculateTableSize(Table table)
      {
         decimal rowsPerPage = 8096/(table.RowSize + 2);
         decimal rowsPerPageComputed = 8096/(table.RowSizeComputed + 2);

         decimal freeRowsPerPage = 8096*((100 - numericUpDown1.Value)/100)/(table.RowSize + 2);
         var freeRowsPerPageComputed = 8096*((100 - numericUpDown1.Value)/100)/(table.RowSizeComputed + 2);

         decimal numLeafPages = table.RowsCount/(rowsPerPage - freeRowsPerPage);
         decimal numLeafPagesCahnged = table.RowsCount/(rowsPerPageComputed - freeRowsPerPageComputed);

         table.TableSize = (long) (8192*numLeafPages);
         table.TableSizeComputed = (long) (8192*numLeafPagesCahnged);


         //row size is too big to calculate precise value
         if (table.TableSize < 0)
         {
            table.TableSize = table.RowSize*table.RowsCount;
         }

         if (table.RowSizeComputed < 0)
         {
            table.TableSize = table.RowSizeComputed * table.RowsCount;
         }

      }

      //Changed value of column size
      private void dataGridViewColumns_CellValueChanged(object sender, DataGridViewCellEventArgs e)
      {
         if (e.RowIndex != -1)
         {
            int newValue = 0;
            try
            {
               
               if (!int.TryParse(dataGridViewColumns[e.ColumnIndex, e.RowIndex].Value.ToString(), out newValue))
               {
                  MessageBox.Show(Resources.WRONG_VALUE);
                  return;
               }
            }
            catch (Exception)
            {
                MessageBox.Show(Resources.WRONG_VALUE);
            }
            if (newValue >= 0)
            {
                tables.ElementAt(listBoxTables.SelectedIndex).Columns.ElementAt(e.RowIndex).EditedSizeOfColumn = newValue;
               
               UpdateTexts();
            }
         }
      }

      //Changed number of rows
      private void numericUpDownRows_ValueChanged(object sender, EventArgs e)
      {
         if (listBoxTables.SelectedIndex < 0)
         {
            return;
         }
         Table table = tables.ElementAt(listBoxTables.SelectedIndex);
          var numericUpDown = sender as NumericUpDown;
          if (numericUpDown != null) table.RowsCount = int.Parse(numericUpDown.Value.ToString(CultureInfo.InvariantCulture));
          CalculateRowSize();
         CalculateTableSize(table);
         UpdateTexts();
      }

      //Convert byte to kB,MB,GB
      private string getValue(long value,Units unit)
      {
         double newValue = 0;
         string unitString = "";
         switch (unit)
         {
            case Units.ByteUnit:
               newValue = value;
               unitString = "bytes";
               break;
            case Units.KilobyteUnit:
               newValue = (double)value / 1024;
               unitString = "kB";
               break;
            case Units.MegabyteUnit:
               newValue = (double)value / 1024 / 1024;
               unitString = "MB";
               break;
            case Units.GigabyteUnit:
               newValue = (double)value / 1024 / 1024 / 1024;
               unitString = "GB";
               break;
         }
         return newValue.ToString(CultureInfo.InvariantCulture) + " " + unitString;
      }

      enum Units
      {
         ByteUnit = 0,
         KilobyteUnit = 1,
         MegabyteUnit = 2,
         GigabyteUnit = 3
      }

      private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
      { 
         UpdateTexts();
      }


      //Recalculate database size
      private void UpdateTexts()
      {
         if (listBoxTables.SelectedIndex < 0)
         {
            return;
         }

         Units unit = (Units) comboBox1.SelectedIndex;
         Table table = tables.ElementAt(listBoxTables.SelectedIndex);


         long databaseSize = 0;
         long databaseSizeComputed = 0;

         CalculateRowSize();
         foreach (var tab in tables)
         {
            CalculateTableSize(tab);
            databaseSize += tab.TableSize;
            databaseSizeComputed += tab.TableSizeComputed;
         }

         labelTableSize.Text = getValue(table.TableSize, unit);
         labelTableSizeComputed.Text = getValue(table.TableSizeComputed, unit);

         textBoxRowSize.Text = getValue(table.RowSize, unit);
         textBoxRowSizeComputed.Text = getValue(table.RowSizeComputed, unit);

         labelDatabaseSize.Text = getValue(databaseSize, unit);
         labelDatabaseSizeComputed.Text = getValue(databaseSizeComputed, unit);
      }


      //Changed fill factor
      private void numericUpDown1_ValueChanged(object sender, EventArgs e)
      {
         if (listBoxTables.SelectedIndex < 0)
         {
            return;
         }
         Table table = tables.ElementAt(listBoxTables.SelectedIndex);
         CalculateRowSize();
         CalculateTableSize(table);
         UpdateTexts();
      }

   }
    
}
