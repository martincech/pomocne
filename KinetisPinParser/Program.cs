﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KinetisPinParser
{
   internal class Program
   {
      private const string PIN_NAME_HEADER = "Pin Name";
      private const string DEFAULT_HEADER = "Default";
      private const string ALT0_HEADER = "ALT0";
      private const string ALT1_HEADER = "ALT1";
      private const string ALT2_HEADER = "ALT2";
      private const string ALT3_HEADER = "ALT3";
      private const string ALT4_HEADER = "ALT4";
      private const string ALT5_HEADER = "ALT5";
      private const string ALT6_HEADER = "ALT6";
      private const string ALT7_HEADER = "ALT7";

      public static void Main(string[] args)
      {
         if (args.Length < 1) {
            PrintHelp();
            return;
         }

         var inputFileName = args[0];
         var outputFileName = inputFileName.Remove(inputFileName.IndexOf('.')) + ".h";
         var output = new StringBuilder();
         MakeFileHeader(output, outputFileName);
         var firstLine = true;
         var indices = new Dictionary<string, int>();
         foreach (var line in File.ReadAllLines(inputFileName)) {
            {
               var values = line.Split('\t', ';', ',');
               if (firstLine) {
                  indices = ParseFirstLine(values.ToList());
                  firstLine = false;
                  if (!CheckValidityOfFirstLine(indices)) {
                     return;
                  }
                  continue;
               }
               int pinNumber;
               var alt = new string[8];


               if (!int.TryParse(ValuesOrEmpty(values, 0), out pinNumber)) {
                  continue;
               }

               var pinName = ValuesOrEmpty(values, indices[PIN_NAME_HEADER]);
               if (!IsValidPinName(pinName)) {
                  continue;
               }

               alt[0] = ValuesOrEmpty(values, indices[ALT0_HEADER]);
               alt[1] = ValuesOrEmpty(values, indices[ALT1_HEADER]);
               alt[2] = ValuesOrEmpty(values, indices[ALT2_HEADER]);
               alt[3] = ValuesOrEmpty(values, indices[ALT3_HEADER]);
               alt[4] = ValuesOrEmpty(values, indices[ALT4_HEADER]);
               alt[5] = ValuesOrEmpty(values, indices[ALT5_HEADER]);
               alt[6] = ValuesOrEmpty(values, indices[ALT6_HEADER]);
               alt[7] = ValuesOrEmpty(values, indices[ALT7_HEADER]);

               var portName = PinNameToPortName(pinName);
               var pin = PinNameToPin(pinName);

               output.AppendLine($"/* {portName}{pin:00} */");
               output.AppendLine($"#define KINETIS_PIN_{portName}{pin:00} PIN_NUMBER(KINETIS_{portName}, {pin})");

               for (var i = 0; i < alt.Length; i++)
               {
                  if (string.IsNullOrEmpty(alt[i]) || alt[i] == "*")
                  {
                     continue;
                  }

                  var altName = alt[i].Replace("/", "_");
                  output.AppendLine($"#define KINETIS_PIN_{altName}_{portName}{pin:00} KINETIS_PIN_{portName}{pin:00}");
                  output.AppendLine($"#define KINETIS_PIN_{altName}_{portName}{pin:00}_FUNCTION {i}");
               }

               output.AppendLine();
            }
         }
         MakeFileFooter(output);
         File.WriteAllText(outputFileName, output.ToString());
      }

      private static bool CheckValidityOfFirstLine(Dictionary<string, int> indices)
      {
         if (!CheckExistenceInDict(indices, PIN_NAME_HEADER)) {
            return false;
         }
         if (!CheckExistenceInDict(indices, DEFAULT_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT0_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT1_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT2_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT3_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT4_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT5_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT6_HEADER))
         {
            return false;
         }
         if (!CheckExistenceInDict(indices, ALT7_HEADER))
         {
            return false;
         }
         return true;
      }

      private static bool CheckExistenceInDict(Dictionary<string, int> indices, string pinNameHeader)
      {
         if (indices.ContainsKey(pinNameHeader) && indices[pinNameHeader] != -1) {
            return true;
         }
         Console.WriteLine($"Missing header column {pinNameHeader}");
         return false;
      }

      private static Dictionary<string, int> ParseFirstLine(IList<string> values)
      {
         var indices = new Dictionary<string, int>();
         AddIndexToDict(values, indices, PIN_NAME_HEADER);
         AddIndexToDict(values, indices, DEFAULT_HEADER);
         AddIndexToDict(values, indices, ALT0_HEADER);
         AddIndexToDict(values, indices, ALT1_HEADER);
         AddIndexToDict(values, indices, ALT2_HEADER);
         AddIndexToDict(values, indices, ALT3_HEADER);
         AddIndexToDict(values, indices, ALT4_HEADER);
         AddIndexToDict(values, indices, ALT5_HEADER);
         AddIndexToDict(values, indices, ALT6_HEADER);
         AddIndexToDict(values, indices, ALT7_HEADER);
         return indices;
      }

      private static void AddIndexToDict(IList<string> values, IDictionary<string, int> indices, string header)
      {
         indices.Add(header, values.IndexOf(values.FirstOrDefault(v => v == header)));
      }

      private static void MakeFileFooter(StringBuilder output)
      {
         output.AppendLine("#endif");
      }

      private static void MakeFileHeader(StringBuilder output, string outputFileName)
      {
         output.AppendLine(CommentLine());
         output.AppendLine("//");
         output.AppendLine($"//    {outputFileName}\t\t{outputFileName.Replace(".h", "")} pin definitions");
         output.AppendLine("//    Version 1.0\t\t(c) Veit Electronics");
         output.AppendLine("//");
         output.AppendLine(CommentLine());
         output.AppendLine();
         output.AppendLine($"#ifndef {HeaderDefineVariable(outputFileName)}");
         output.AppendLine($"#define {HeaderDefineVariable(outputFileName)}");
         output.AppendLine("");
      }

      private static string CommentLine()
      {
         return "//" + new string('*', 78);
      }

      private static string HeaderDefineVariable(string outputFileName)
      {
         return $"__{outputFileName.Replace(".h", "")}_H__".ToUpper();
      }

      private static string ValuesOrEmpty(IReadOnlyList<string> values, int index)
      {
         return values.Count > index? values[index].TrimStart(' ').TrimEnd(' ') : "";
      }

      private static void PrintHelp()
      {
         Console.WriteLine("KinetisPinParser.exe VstupniSoubor VystupniSoubor");
      }

      private static bool IsValidPinName(string pinName)
      {
         return pinName.StartsWith("PT");
      }

      private static string PinNameToPortName(string pinName)
      {
         return "PORT" + pinName[2];
      }

      private static int PinNameToPin(string pinName)
      {
         var parsed = 0;
         var pin = 0;
         if (pinName.Length >= 4 && char.IsDigit(pinName[3])) {
            if (int.TryParse(pinName[3].ToString(), out parsed)) {
               pin = parsed;
            }
         }

         if (pinName.Length >= 5 && char.IsDigit(pinName[4])) {
            if (int.TryParse(pinName[4].ToString(), out parsed))
            {
               pin = pin * 10 + parsed;
            }
            
         }

         return pin;
      }
   }
}
