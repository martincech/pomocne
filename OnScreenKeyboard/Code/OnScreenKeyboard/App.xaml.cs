﻿using System.Diagnostics;
using System.Windows;
using OnScreenKeyboard.Applications;
using OnScreenKeyboard.Presentations;

namespace OnScreenKeyboard
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      protected override void OnStartup(StartupEventArgs e)
      {
         var thisProc = Process.GetCurrentProcess();
         if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
         {
            // Application is already running
            Current.Shutdown();
            return;
         }

         base.OnStartup(e);
         MainWindow = new MainWindow {DataContext = new MainWindowViewModel()};
         MainWindow.Show();
      }
   }
}
