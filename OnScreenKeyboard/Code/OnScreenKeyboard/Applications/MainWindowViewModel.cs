﻿using System.Runtime.InteropServices;
using System.Windows.Input;
using Desktop.Wpf.Applications;
using OnScreenKeyboard.Helpers;
using MvvmFramework.Observable;

namespace OnScreenKeyboard.Applications
{
   public class MainWindowViewModel : ObservableObject
   {
      [DllImport("User32.dll")]
      public static extern void keybd_event(byte bVK, byte bScan, int dwFlags, int dwExtraInfo);

      private ICommand buttonCommand;

      public ICommand ButtonCommand
      {
         get
         {
            if (buttonCommand == null)
            {
               buttonCommand = new RelayCommand<string>(
                  // execute
                  sender =>
                  {
                     switch (sender)
                     {
                        case "Cmd1":
                           AddKeyBoardINput(KeyE.Num1);
                           break;
                        case "Cmd2":
                           AddKeyBoardINput(KeyE.Num2);
                           break;
                        case "Cmd3":
                           AddKeyBoardINput(KeyE.Num3);
                           break;
                        case "Cmd4":
                           AddKeyBoardINput(KeyE.Num4);
                           break;
                        case "Cmd5":
                           AddKeyBoardINput(KeyE.Num5);
                           break;
                        case "Cmd6":
                           AddKeyBoardINput(KeyE.Num6);
                           break;
                        case "Cmd7":
                           AddKeyBoardINput(KeyE.Num7);
                           break;
                        case "Cmd8":
                           AddKeyBoardINput(KeyE.Num8);
                           break;
                        case "Cmd9":
                           AddKeyBoardINput(KeyE.Num9);
                           break;
                        case "Cmd0":
                           AddKeyBoardINput(KeyE.Num0);
                           break;
                        case "CmdBackspace":
                           AddKeyBoardINput(KeyE.Backspace);
                           break;
                        case "CmdScan":
                           AddKeyBoardINput(KeyE.Scan);
                           break;
                        case "CmdBack":
                           AddKeyBoardINput(KeyE.Back, true);
                           break;
                        case "CmdMenu":
                           AddKeyBoardINput(KeyE.Menu, true);
                           break;
                        case "CmdHome":
                           AddKeyBoardINput(KeyE.Home);
                           break;
                        case "CmdCallOn":
                           AddKeyBoardINput(KeyE.CallOn);
                           break;
                        case "CmdCallOff":
                           AddKeyBoardINput(KeyE.CallOff);
                           break;
                        case "CmdOk":
                           AddKeyBoardINput(KeyE.Ok);
                           break;
                        case "CmdUpArrow":
                           AddKeyBoardINput(KeyE.ArrowUp);
                           break;
                        case "CmdLeftArrow":
                           AddKeyBoardINput(KeyE.ArrowLeft);
                           break;
                        case "CmdRightArrow":
                           AddKeyBoardINput(KeyE.ArrowRight);
                           break;
                        case "CmdDownArrow":
                           AddKeyBoardINput(KeyE.ArrowDown);
                           break;
                        case "CmdF1":
                           AddKeyBoardINput(KeyE.F1);
                           break;
                        case "CmdF2":
                           AddKeyBoardINput(KeyE.F2);
                           break;
                        case "CmdF3":
                           AddKeyBoardINput(KeyE.F3);
                           break;
                        case "CmdF4":
                           AddKeyBoardINput(KeyE.F4);
                           break;
                        case "CmdDot":
                           AddKeyBoardINput(KeyE.Dot);
                           break;
                        case "CmdClose":
                           System.Windows.Application.Current.Shutdown();
                           break;
                     }
                  },
                  // canExecute
                  param => true
                  );
            }
            return buttonCommand;
         }
      }

      private static void AddKeyBoardINput(KeyE input, bool ctrl = false)
      {
         const byte keyUp = 0x02;
         if (ctrl)
         {
            keybd_event((byte) KeyE.Ctrl, 0, 0, 0);
            keybd_event((byte) input, 0, 0, 0);

            keybd_event((byte)KeyE.Ctrl, 0, keyUp, 0);
            keybd_event((byte)input, 0, keyUp, 0);
         }
         else
         {
            keybd_event((byte) input, 0, 0, 0);
            keybd_event((byte) input, 0, keyUp, 0);
         }
      }
   } 
}
